# Transfer.sh Cloudron App

This repository contains the Cloudron app package source for [transfer.sh](-https://github.com/dutchcoders/transfer.sh).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=sh.transfer.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id sh.transfer.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd transfer.sh

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore.

```
cd transfer/test

npm install
mocha --bail test.js
```

