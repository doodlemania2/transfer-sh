#!/bin/bash
set -eu
chown -R cloudron:cloudron /app/data
cd /app/code
exec /usr/local/bin/gosu cloudron:cloudron ./transfersh --provider=local --temp-path=/tmp/ --basedir=/app/data/ --listener :8080 --web-path=/app/html/
