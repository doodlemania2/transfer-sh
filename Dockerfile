FROM cloudron/base:0.10.0
MAINTAINER Derek Martin <me@derekmartin.org>

EXPOSE 8080

RUN export PATH=$PATH:/usr/local/go-1.7.5/bin && \
 export GOPATH=/usr/local/go-1.7.5/bin && \
 export GOROOT=/usr/local/go-1.7.5 && \
 mkdir -p /app/code && \
 mkdir -p /usr/local/go-1.7.5/src/github.com/dutchcoders/transfer.sh && \
 cd /usr/local/go-1.7.5/src/github.com/dutchcoders/transfer.sh && \
 curl -L https://github.com/dutchcoders/transfer.sh/tarball/7bf499b0922f9b6a48c8832e900bcd88492612a4 | tar -xz --strip-components 1 -f - && \
 cd /app/code && \
 go build -o transfersh /usr/local/go-1.7.5/src/github.com/dutchcoders/transfer.sh/main.go

RUN mkdir -p /app/html && \
cd /app/html && \
curl -L https://git.cloudron.io/doodlemania2/transfer-sh/raw/60063f58625e8b465c3930ce7b473865174511b4/html.zip -o html.zip && \
unzip html.zip && \
mv dist/* . && \
rm -rf dist && \
rm html.zip


WORKDIR /app/code/
COPY start.sh /app/code/
RUN chown -R cloudron.cloudron /app/code && chmod +x /app/code/transfersh && chmod +x /app/code/start.sh

CMD ["/app/code/start.sh"]
