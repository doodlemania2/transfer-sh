This app packages transfer.sh from Dutch Coders

transfer.sh is a simple tool that provides easy and fast file sharing from the command-line.

### Features

**Made for use with Linux shells

Uploads are as easy as sending a curl command:

    curl --upload-file ./hello.txt https://sub.domain.com/hello.txt

Returns something like this:

    https://sub.domain.com/66nb8/hello.txt

Files are kept for 14 days and then removed.

**Easy file sharing

Once you upload your file, the generated URL can be emailed/IM'd/curl'ed on the other computer easily.

**Support for encryption first

    cat /hello.txt | gpg -ac -o-|curl -X PUT --upload-file "-" https://sub.domain.com/hello.txt
    curl https://sub.domain.com/1lDau/hello.txt|gpg -o- > hello1.txt

### Cloudron Notes

**No users

The application has no concept of authentication or users, it is an open endpoint for file transfers.
